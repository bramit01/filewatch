@echo off
title IDB Java Batch Monitoring
color 57

echo. | date | FIND "(mm" > NUL
If errorlevel 1,(call :Parsedate DD MM) Else,(call :Parsedate MM DD)
goto :EOF
:Parsedate ----------------------------------------------------------
For /F "tokens=1-4 delims=/.- " %%A in ('date /T') do if %%D!==! (
 set %1=%%A&set %2=%%B&set YYYY=%%C
) else (
 set DOW=%%A&set %1=%%B&set %2=%%C&set YYYY=%%D)


SET YEAR=%YYYY%
SET YEARMONTH=%YYYY%%MM%
SET YEARMONTHDAY=%YYYY%%MM%%DD%

cls
echo.
echo %DATE% %TIME%
echo %CDATE%
echo logs\idb\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-jidb_A.wsf.log
echo.
\\grsfrs\boxi_batch_test$\monitor\tail -f -q -n 1 \\grsfrs\boxi_batch_test$\logs\idb\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-stream_A.wsf.log
pause