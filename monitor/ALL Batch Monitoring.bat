@echo off
title GFS Java Batch Monitoring
color 27

echo. | date | FIND "(mm" > NUL
If errorlevel 1,(call :Parsedate DD MM) Else,(call :Parsedate MM DD)
goto :EOF
:Parsedate ----------------------------------------------------------
For /F "tokens=1-4 delims=/.- " %%A in ('date /T') do if %%D!==! (
 set %1=%%A&set %2=%%B&set YYYY=%%C
) else (
 set DOW=%%A&set %1=%%B&set %2=%%C&set YYYY=%%D)


SET YEAR=%YYYY%
SET YEARMONTH=%YYYY%%MM%
SET YEARMONTHDAY=%YYYY%%MM%%DD%

cls
echo.
echo %DATE% %TIME%
echo %CDATE%
echo logs\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-stream_A.wsf.log
echo.
\\grsfrs\boxi_batch_test$\monitor\snaketail \\grsfrs\boxi_batch_test$\logs\gfs\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-stream_A.wsf.log \\grsfrs\boxi_batch_test$\logs\regflex\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-stream_A.wsf.log \\grsfrs\boxi_batch_test$\logs\risk\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-stream_A.wsf.log \\grsfrs\boxi_batch_test$\logs\irlbank\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-stream_A.wsf.log \\grsfrs\boxi_batch_test$\logs\idb\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-stream_A.wsf.log \\grsfrs\boxi_batch_test$\logs\other\%YEAR%\%YEARMONTH%\%YEARMONTHDAY%\%YEARMONTHDAY%_fw-stream_A.wsf.log
pause