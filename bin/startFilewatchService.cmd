
pushd \\grsfrs\boxi_batch_test$\bin
echo %~f0 %* >> ..\logs\startFilewatchService.log
d:\java8\bin\java -Dlogback.configurationFile=filewatchservice_logback.xml -cp filewatchservice-1.0.1-RELEASE.jar;lib\* com.rabobank.grs.filewatch.FilewatchServiceMain %*
popd