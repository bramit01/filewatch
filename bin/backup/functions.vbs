'pause function
'if file exists waits until it is removed
Function waitIfExists (file, withRepeat)
		Dim fso
		Set fso = CreateObject ("scripting.filesystemobject")

    ' Sleeps until the file exists
    ' The polling interval will increase gradually, but never rises above MAX_WAITTIME
    ' Times out after TIMEOUT msec. Will return false if caused by timeout.
    Dim waittime, totalwaittime, rep, doAgain, showLog
    Const INIT_WAITTIME = 20
    Const MAX_WAITTIME = 1000
    Const TIMEOUT = 50000
    Const SLOPE = 1.1
    doAgain  = true
    showLog = true
    Do While doAgain

        waittime = INIT_WAITTIME
        totalwaittime = 0

        Do While totalwaittime < TIMEOUT
            waittime = Int (waittime * SLOPE)
            If waittime>MAX_WAITTIME Then waittime=MAX_WAITTIME
            totalwaittime = totalwaittime + waittime

            WScript.sleep waittime
            If Not fso.fileExists (file) Then
                waitTilExists = true
                Exit Function
            Else
       	        If showLog Then
       	        	WL strLogFile, Section, "PAUSE", "Waiting for file: [" & file & "] to be removed, timeout " & TIMEOUT 
       	        	showLog = false
								End If
            End If
        Loop
        If withRepeat Then
            'rep = MsgBox ("This file does not exist:" & vbcr & file & vbcr & vbcr & "Keep trying?", vbRetryCancel+vbExclamation, "File not found")
            doAgain = true
        Else
            doAgain = false
        End If
    Loop

    waitTilExists = false
End Function

Function GetFileNameWithoutExtension(FileName)
	Dim Result, i
	Result = FileName
	i = inStrRev(FileName, ".")
	If (i>0) Then
		Result = Mid(FileName, 1, i-1)
	End If
	GetFileNameWithoutExtension = Result
End Function
'=========================================================================
' MappedSharepointDrive, returns folderlocation, or mappedsharedpointdrive s:\
' 20140421:JdW: added
'=========================================================================

Function MappedSharepointDrive (destinationfolder)
On Error Resume Next
	Dim strSearch:strSearch = "\\sharepoint.rabonet.com"	
	Dim strDestination:strDestination = ""
	'wscript.echo destinationfolder
	
	If InStr(destinationfolder,strSearch) <> -1 then
		'wscript.echo "found"
		'found sharepoint folder
		'remove s; drive just to be sure	
		WshNetwork.RemoveNetworkDrive "S:"
		'�gnore errors
		If err.number <> 0 Then
			err.clear
		End If
		'wscript.echo "removed"
		'map s: drive to sharepointfolder (with lon.busobj credentials)
		'wscript.echo DecryptHex("%72%61%62%6F%6E%65%74%65%75%5C%6C%6F%6E%2E%62%75%73%6F%62%6A")
		'wscript.echo DecryptHex("%42%75%73%69%6E%65%24%24%6F%62%6A%65%63%74%32")		
		WshNetwork.MapNetworkDrive "S:", destinationfolder, False, DecryptHex("%72%61%62%6F%6E%65%74%65%75%5C%6C%6F%6E%2E%62%75%73%6F%62%6A"), DecryptHex("%42%75%73%69%6E%65%24%24%6F%62%6A%65%63%74%32")

  	If err.number <> 0 then
  		'in case of error, destination is our test destination folder	
  		strDestination = ReplaceAllOccurences(destinationfolder, "\\sharepoint.rabonet.com\", getini("General", "tempSharepointFolder", "", sIniFile))
  		'wscript.echo strDestination
  		WriteLineToFile strGroupTriggerLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";Could not map sharepoint location [" & destinationfolder & "],error: " & err.description & ",  destinationfolder will be [" & strDestination & "]" 
			WL strLogfile, Section, "ERROR","Could not map sharepoint location [" & destinationfolder & "],error: " & err.description & ",  destinationfolder will be [" & strDestination & "]"
  	Else
  		strDestination = "S:\"
  	End if
  	
	Else
		'sharepointfolder not found, use originaldestination folder
		strDestination = destinationfolder
	End if 

  MappedSharepointDrive = strDestination
  On Error Goto 0
End Function

'=========================================================================
' DecryptHex, decrypt function als used in functions.inc (mustr)
' 20140416:JdW: added
'=========================================================================
Function DecryptHex(str)
	dim strD, i, hv
	strD = ""
	for i = 2 to Len(str)
		hv = ""
		while Mid(str, i, 1) <> "%" and i <= Len(str)
			hv = hv + Mid(str, i, 1)
			i = i+1
		wend
		strD = strD + chr(CLng("&h" & hv))
	next
	DecryptHex = strD
End Function
'=========================================================================
' RestartWithCScript32, start script with 32bit version of cscript.exer on 64bits systems

' 20140416:JdW: added
'=========================================================================
   ' check to see if we are on 64bit OS -> re-run this script with 32bit cscript
   Function RestartWithCScript32(extraargs)
   Dim strCMD, iCount
   strCMD = r32wShell.ExpandEnvironmentStrings("%SYSTEMROOT%") & "\SysWOW64\cscript.exe"
   If NOT r32fso.FileExists(strCMD) Then strCMD = "cscript.exe" ' This may not work if we can't find the SysWOW64 Version
   strCMD = strCMD & Chr(32) & Wscript.ScriptFullName & Chr(32)
   If Wscript.Arguments.Count > 0 Then
    For iCount = 0 To WScript.Arguments.Count - 1
     if Instr(Wscript.Arguments(iCount), " ") = 0 Then ' add unspaced args
      strCMD = strCMD & " " & Wscript.Arguments(iCount) & " "
     Else
      If Instr("/-\", Left(Wscript.Arguments(iCount), 1)) > 0 Then ' quote spaced args
       If InStr(WScript.Arguments(iCount),"=") > 0 Then
        strCMD = strCMD & " " & Left(Wscript.Arguments(iCount), Instr(Wscript.Arguments(iCount), "=") ) & """" & Mid(Wscript.Arguments(iCount), Instr(Wscript.Arguments(iCount), "=") + 1) & """ "
       ElseIf Instr(WScript.Arguments(iCount),":") > 0 Then
        strCMD = strCMD & " " & Left(Wscript.Arguments(iCount), Instr(Wscript.Arguments(iCount), ":") ) & """" & Mid(Wscript.Arguments(iCount), Instr(Wscript.Arguments(iCount), ":") + 1) & """ "
       Else
        strCMD = strCMD & " """ & Wscript.Arguments(iCount) & """ "
       End If
      Else
       strCMD = strCMD & " """ & Wscript.Arguments(iCount) & """ "
      End If
     End If
    Next
   End If
   r32wShell.Run strCMD & " " & extraargs, 0, False
   End Function
   'add this at the top of every script
   'Dim r32wShell, r32env1, r32env2, r32iCount
   'Dim r32fso
   'SET r32fso = CreateObject("Scripting.FileSystemObject")
   'Set r32wShell = WScript.CreateObject("WScript.Shell")
   'r32env1 = r32wShell.ExpandEnvironmentStrings("%PROCESSOR_ARCHITECTURE%")
   'If r32env1 <> "x86" Then ' not running in x86 mode
   ' For r32iCount = 0 To WScript.Arguments.Count - 1
   '  r32env2 = r32env2 & WScript.Arguments(r32iCount) & VbCrLf
   ' Next
   ' If InStr(r32env2,"restart32") = 0 Then RestartWithCScript32 "restart32" Else MsgBox "Cannot find 32bit version of cscript.exe or unknown OS type " & r32env1
   ' Set r32wShell = Nothing
   ' WScript.Quit
   'End If
   'Set r32wShell = Nothing
   'Set r32fso = Nothing
   ' *******************
   ' *** END 64bit check
   ' *******************
   '=========================================================================
' TXT2CSV, create a csv from a .txt file if flag is equal to 1
' existing commas in the txt file will be removed first, then all tabs
' will be replaced by commas.
' The output of the function is the (new) filename that can be used by the script
' 20140113:JdW: Moved function to functions.vbs
' 20140113:JdW: Added LCase in check for .txt and .csv
'=========================================================================
Function TXT2CSV(filename,flag)
            On Error Resume Next
            TXT2CSV=filename
            If flag = "" Then Exit Function
' 20140113:JdW: Added LCase in check for .txt and .csv
            If LCase(Right(filename,4)) <> ".txt" And LCase(Right(filename,4)) <> ".csv" Then Exit Function
            Const ForReading = 1
            Const ForWriting = 2
            Dim objFSO:Set objFSO = CreateObject("Scripting.FileSystemObject")

            If objFSO.FileExists(filename) Then
                        Dim objFile:Set objFile = objFSO.OpenTextFile(filename, ForReading)
                        Dim strText:strText = objFile.ReadAll
                        objFile.Close

                        Dim strNewText
                        strNewText = Replace(strText, flag, "")
                        strNewText = Replace(strNewText, Chr(9), flag)

                        filename2 = Replace(filename, ".txt", ".csv")
                        If Not objFSO.FileExists(filename2) Then
                                    Set objFile = objFSO.CreateTextFile(filename2)
                        Else
                                    Set objFile = objFSO.OpenTextFile(filename2, ForWriting)
                        End If
                        objFile.Write strNewText
                        objFile.Close
                        If filename <> filename2 Then
                                    objFSO.DeleteFile(filename)
                        End If
                        TXT2CSV=filename2
            End If
						On Error Goto 0
End Function


'===============================================================================
' create a date from a timestamp
'===============================================================================
Function ConvertStringToTimeStamp(strDateStamp)
            If Len(strDateStamp)=15 Then
                        ConvertStringToTimeStamp=DateSerial(Mid(strDateStamp,1,4),Mid(strDateStamp,5,2),Mid(strDateStamp,7,2)) + TimeSerial(Mid(strDateStamp,10,2),Mid(strDateStamp,12,2),Mid(strDateStamp,14,2))
            Else
                        ConvertStringToTimeStamp=Now()
            End If
End Function

'===============================================================================
' Write a Log message to a file with a RunID and Timestamp
'===============================================================================
Sub WriteLogMessage(Path, Errormsg, RunID)
            On Error Resume Next
            Dim objFSA, objFileA

            Set objFSA = CreateObject("Scripting.FileSystemObject")
            If objFSA.FileExists(Path) = False Then
                        Set objFileA = objFSA.CreateTextFile(Path)
            Else
                        Set objFileA = objFSA.OpenTextFile(Path,8,0)
            End If

            objFileA.WriteLine RunID & ";" & FormatDate(Now, "yyyymmdd_hhnnss") & ";" & Errormsg
            objFileA.Close
            Set objFileA = Nothing
            Set objFSA = Nothing
						On Error Goto 0
End Sub

'===============================================================================
' Write a Line to a file
'===============================================================================
Sub WriteLineToFile(Path, msg)
On Error Resume Next
            Dim objFSB,objFileB
            If Path<>"" Then
                        Set objFSB = CreateObject("Scripting.FileSystemObject")
                        If Not objFSB.FileExists(Path) Then
                                    Set objFileB = objFSB.CreateTextFile(Path)
                        Else
                                    Set objFileB = objFSB.OpenTextFile(Path,8,0)
                        End If
                        If msg<>"" Then
                                    objFileB.WriteLine msg
                        End If
                        objFileB.Close
                        Set objFileB = Nothing
                        Set objFSB = Nothing
            End If
On Error Goto 0
End Sub


'===============================================================================
' Write a to a file NO LINEFEED
'===============================================================================
Sub WriteToFile(Path, msg)
            Dim objFSB,objFileB
            If Path<>"" Then
                        Set objFSB = CreateObject("Scripting.FileSystemObject")
                        If Not objFSB.FileExists(Path) Then
                                    Set objFileB = objFSB.CreateTextFile(Path)
                        Else
                                    Set objFileB = objFSB.OpenTextFile(Path,8,0)
                        End If
                        If msg<>"" Then
                                    objFileB.Write msg
                        End If
                        objFileB.Close
                        Set objFileB = Nothing
                        Set objFSB = Nothing
            End If
End Sub

'===============================================================================
' geeft een sorteerbare "unieke" datetimestring
'===============================================================================
Function GetDateFromRunID(RunID)
            ' runID is like 20080827_144008_RiskIT_Trigger01
            GetDateFromRunID=DateAdd("s",Mid(RunID,10,2)*3600 + Mid(RunID,12,2)*60 + Mid(RunID,14,2),DateSerial(Mid(RunID,1,4),Mid(RunID,5,2),Mid(RunID,7,2)))
End Function

'===============================================================================
' Returns a (sub)path like 2006\200605\20060531
'===============================================================================
Function GenerateDateSubfolderStructure (timestamp)
            Dim maand:maand=LeadingZero(Month(timestamp),2)
            Dim dag:dag=LeadingZero(Day(timestamp),2)
            GenerateDateSubfolderStructure = Year(timestamp) & "\" & Year(timestamp) & maand & "\" & Year(timestamp) & maand & dag
End Function

'===============================================================================
' Returns a (sub)path like 2006\200605
'===============================================================================
Function GenerateYearMonthSubfolderStructure (timestamp)
            Dim maand:maand=LeadingZero(Month(timestamp),2)
            GenerateYearMonthSubfolderStructure = Year(timestamp) & "\" & Year(timestamp) & maand
End Function


'===============================================================================
' Adds Leading Zero to a number
'===============================================================================
Function LeadingZero(Value, Numbers)
            If Len(Value) < Numbers Then
                        LeadingZero = String(Numbers - Len(Value), "0") & Value
            Else
                        LeadingZero = Value
            End If
End Function

'===============================================================================
' Check if folder exists, if not, try to create it
' returns 1 if it exists, 0 if it isn't or has failed to create
'===============================================================================
Function CheckFolder(strFolder)
            On Error Resume Next
    'Check if folder exists, if not, try to create missing subfolder(s)
    Dim PathToEvaluate, PathToSearch, StartChars
    Dim StartPos, SearchPos
    Dim vFolders
    Dim v
            Dim fso, f
            Set fso = CreateObject("Scripting.FileSystemObject")
            Set f = fso.CreateFolder(strFolder)

    'Initial value is false
    CheckFolder = 0
    'Remove trailing slash to prevent compare/split problems later on
    If Right(strFolder, 1) = "\" Then
        strFolder = Mid(strFolder, 1, Len(strFolder) - 1)
    End If
    'Folder doesn't exist, so break down in an array and recreate subfolder by subfolder
            If Not (fso.FolderExists(strFolder)) Then
        If Mid(strFolder, 1, 2) = "\\" Then
            'PathToEvaluate is UNC, there must be more slashes than 2
            StartPos = 3
            PathToEvaluate = "\"
        ElseIf Mid(strFolder, 3, 1) = "\" Then
            'PathToEvaluate is drivemapping, there could only be a root folder in strFolder (i.e. c:\temp)
            StartPos = 4
            If InStr(Mid(strFolder, StartPos), "\") = 0 Then
                Set f = fso.CreateFolder(strFolder)
            End If
            PathToEvaluate = Mid(strFolder, 1, 2)
        Else
        		On Error Goto 0
            Exit Function
        End If
        vFolders = Split(Mid(strFolder, StartPos), "\")
        For Each v In vFolders
            PathToEvaluate = PathToEvaluate & "\" & v
            If Len(Dir(PathToEvaluate, vbDirectory)) = 0 Then
                Set f = fso.CreateFolder(PathToEvaluate)
            End If
        Next
    End If
    If (fso.FolderExists(strFolder)) Then
        CheckFolder = 1
    End If
            err.clear
            
    On Error Goto 0
End Function

'===============================================================================
' Search/replace function
'===============================================================================
Function ReplaceAllOccurences(StrStringToClean, StrValueToLookFor, StrValueToReplace)
    Dim strTempStorage
    Dim StartInt, EndInt, length, LengthToLookFor

    length = Len(StrStringToClean)
    LengthToLookFor = Len(StrValueToLookFor)
    StartInt = 1
    EndInt = StartInt + LengthToLookFor - 1
    Do While EndInt <= length
       If Mid(StrStringToClean, StartInt, LengthToLookFor) = StrValueToLookFor Then
          strTempStorage = strTempStorage & StrValueToReplace
          StartInt = StartInt + LengthToLookFor
          EndInt = StartInt - 1
       Else
          strTempStorage = strTempStorage & Mid(StrStringToClean, StartInt, 1)
          StartInt = StartInt + 1
          EndInt = StartInt + LengthToLookFor - 1
       End If
    Loop
    If StartInt <= length Then
       strTempStorage = strTempStorage & Mid(StrStringToClean, StartInt, length - StartInt + 1)
    End If

    ReplaceAllOccurences = strTempStorage
End Function

Function FormatDate(strDate, strDateFmt)
            On Error Resume Next
            Dim strRet
            Dim i
            Dim formatBlock
            Dim formatLength
            Dim charLast
            Dim charCur
            strDateFmt=LCase(strDateFmt)
            formatLength = Len(strDateFmt)

            for i = 1 to formatLength + 1
                        ' grab the current character
                        charCur = Mid(strDateFmt, i, 1)

                        if charCur = charLast then
                            ' The block is not finished.  Continue growing the block and iterate to the next character.
                            formatBlock = formatBlock & charCur
                        else
                            ' we have a change and need to handle the previous block
                            Select Case formatBlock
                            Case "hh"
                                    strRet = strRet & LeadingZero(DatePart("h",strDate),2)
                            Case "nn"     'm is reserved for months
                                    strRet = strRet & LeadingZero(DatePart("n",strDate),2)
                            Case "ss"
                                    strRet = strRet & LeadingZero(DatePart("s",strDate),2)
                            Case "mmmm"
                                strRet = strRet & MonthName(DatePart("m",strDate),False)
                            Case "mmm"
                                strRet = strRet & MonthName(DatePart("m",strDate),True)
                            Case "mm"
                                strRet = strRet & Right("0" & DatePart("m",strDate),2)
                            Case "m"
                                strRet = strRet & DatePart("m",strDate)
                            Case "dddd"
                                strRet = strRet & WeekDayName(DatePart("w",strDate,1),False)
                            Case "ddd"
                                strRet = strRet & WeekDayName(DatePart("w",strDate,1),True)
                            Case "dd"
                                strRet = strRet & right("0" & DatePart("d",strDate),2)
                            Case "d"
                                strRet = strRet & DatePart("d",strDate)
                            Case "o"
                                strRet = strRet & intToOrdinal(DatePart("d",strDate))
                            Case "yyyy"
                                strRet = strRet & DatePart("yyyy",strDate)
                            Case "yy"
                                strRet = strRet & right(DatePart("yyyy",strDate),2)
                            Case "y"
                                strRet = strRet & cInt(right(DatePart("yyyy",strDate),2))
                            Case "www"
                                    strRet = strRet & "w" & LeadingZero(cInt(right(DatePart("ww",strDate,2,2),2)),2)
                            Case "ww"
                                    strRet = strRet & LeadingZero(cInt(right(DatePart("ww",strDate,2,2),2)),2)
                            Case Else
                                strRet = strRet & formatBlock
                            End Select
                            ' Block handled.  Now reset the block and continue iterating to the next character.
                            formatBlock = charCur
                        End If

                        charLast = charCur
            Next 'i
            FormatDate = strRet
            On Error Goto 0
End Function

'===============================================================================
' Process a string and replace dates and times
'===============================================================================
Public Function SubstDateTimeVars(StrStringToProcess)
            SubstDateTimeVars = StrStringToProcess
    If InStr(SubstDateTimeVars,"_BUSINESSDAY") Then
                        Dim strHolidays
                        strHolidays = getini("BusinessDays", "HolidaysUTC", "", sIniFile)
            If Left(SubstDateTimeVars,1) = "|" Then
                        If Mid(SubstDateTimeVars,5,1) = "|" Then 
                                    strHolidays = getini("BusinessDays", "Holidays" & Mid(SubstDateTimeVars,2,3), strHolidays, sIniFile)
                                    SubstDateTimeVars = Trim(Mid(SubstDateTimeVars,6,Len(SubstDateTimeVars)))
                                    WriteLineToFile strLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";strHolidays=" & strHolidays
                        End If
            End If
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_DD-MMM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "dd-mmm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENT_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, 0, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_M%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "M"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_MM%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "MM"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_MMM%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "MMM"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS2_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, -2, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS2_BUSINESSDAY_DD-MMM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, -2, strHolidays), "dd-mmm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, 1, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT2_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, 2, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT5_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, 5, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT6_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, 6, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT10_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, 10, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT20_BUSINESSDAY_DD-MM-YYYY%", FormatDate(dtAddSubtractBusinessDay(Now, 20, strHolidays), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENT_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 0, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENT_BUSINESSDAY_YYYY-MM-DD%", FormatDate(dtAddSubtractBusinessDay(Now, 0, strHolidays), "YYYY-MM-DD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS2_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, -2, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS2_BUSINESSDAY_YYYY-MM-DD%", FormatDate(dtAddSubtractBusinessDay(Now, -2, strHolidays), "YYYY-MM-DD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_YYYY/MM/DD%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "YYYY/MM/DD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_BUSINESSDAY_YYYY-MM-DD%", FormatDate(dtAddSubtractBusinessDay(Now, -1, strHolidays), "YYYY-MM-DD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 1, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT2_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 2, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT3_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 3, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT4_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 4, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT5_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 5, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT10_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 10, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT20_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 20, strHolidays), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%NEXT20_BUSINESSDAY_YYYYMMDD%", FormatDate(dtAddSubtractBusinessDay(Now, 20, strHolidays), "YYYYMMDD"))
    End If          
    If InStr(SubstDateTimeVars, "%PREVIOUS") Then
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_DAY_YYYYMMDD%", FormatDate(DateAdd("d", -1, Now), "YYYYMMDD"))
								SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_DAY_MM/DD/YYYY%", FormatDate(DateAdd("d", -1, Now), "MM/DD/YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_DAY_YYYY-MM-DD%", FormatDate(DateAdd("d", -1, Now), "YYYY-MM-DD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS7_DAY_YYYY-MM-DD%", FormatDate(DateAdd("d", -7, Now), "YYYY-MM-DD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS7_DAY_DD-MM-YYYY%", FormatDate(DateAdd("d", -7, Now), "DD-MM-YYYY"))
								SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS7_DAY_MM/DD/YYYY%", FormatDate(DateAdd("d", -7, Now), "MM/DD/YYYY"))
								SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS7_DAY_DD/MM/YYYY%", FormatDate(DateAdd("d", -7, Now), "DD/MM/YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_DAY_DD-MM-YYYY%", FormatDate(DateAdd("d", -1, Now), "DD-MM-YYYY"))        
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_DAY_DD/MM/YYYY%", FormatDate(DateAdd("d", -1, Now), "DD/MM/YYYY"))        
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUS_DAY_MM%", FormatDate(DateAdd("d", -1, Now), "MM"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_M%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "m"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_MM%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "mm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_MM-YYYY%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_YYYY-MM%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "yyyy-mm"))
                                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_YYYY-M%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "yyyy-m"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_MMMM%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "Mmmm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_MMM%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "Mmm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_YY%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "YY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_DD%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "dd"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH_YYYY%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(Now, "mmm") & "-" & Year(Now)), "yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-2_YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -2, Now), "mmm") & "-" & Year(DateAdd("m", -2, Now)), "yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-3_YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -3, Now), "mmm") & "-" & Year(DateAdd("m", -3, Now)), "yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-4_YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -4, Now), "mmm") & "-" & Year(DateAdd("m", -4, Now)), "yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-5_YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -5, Now), "mmm") & "-" & Year(DateAdd("m", -5, Now)), "yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-6_YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -6, Now), "mmm") & "-" & Year(DateAdd("m", -6, Now)), "yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-2_MM%", FormatDate("01-" & FormatDate(DateAdd("m", -2, Now), "mmm") & "-" & Year(DateAdd("m", -2, Now)), "mm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-3_MM%", FormatDate("01-" & FormatDate(DateAdd("m", -3, Now), "mmm") & "-" & Year(DateAdd("m", -3, Now)), "mm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-4_MM%", FormatDate("01-" & FormatDate(DateAdd("m", -4, Now), "mmm") & "-" & Year(DateAdd("m", -4, Now)), "mm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-5_MM%", FormatDate("01-" & FormatDate(DateAdd("m", -5, Now), "mmm") & "-" & Year(DateAdd("m", -5, Now)), "mm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSMONTH-6_MM%", FormatDate("01-" & FormatDate(DateAdd("m", -6, Now), "mmm") & "-" & Year(DateAdd("m", -6, Now)), "mm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%PREVIOUSYEARLASTDAY_DD-MM-YYYY%", "31-12-" & Year(Now)-1)
            End If
    If InStr(SubstDateTimeVars, "%FIRST") Then
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYPREVIOUSQUARTER_DD-MM-YYYY%", FormatDate(DateAdd("m", -3, DateSerial(Year(NOW), Int((Month(NOW) - 1) / 3) * 3 + 1, 1)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYPREVIOUSMONTH_DD-MM-YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -1, Now), "mmm") & "-" & Year(DateAdd("m", -1, Now)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYPREVIOUSMONTH_MM/DD/YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -1, Now), "mmm") & "-" & Year(DateAdd("m", -1, Now)), "mm/dd/yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYPREVIOUSMONTH_DD/MM/YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -1, Now), "mmm") & "-" & Year(DateAdd("m", -1, Now)), "dd/mm/yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYPREVIOUSMONTH-11_DD-MM-YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -12, Now), "mmm") & "-" & Year(DateAdd("m", -12, Now)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYMONTH-2_DD-MM-YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -2, Now), "mmm") & "-" & Year(DateAdd("m", -2, Now)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYMONTH-3_DD-MM-YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -3, Now), "mmm") & "-" & Year(DateAdd("m", -3, Now)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYMONTH-4_DD-MM-YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -4, Now), "mmm") & "-" & Year(DateAdd("m", -4, Now)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYMONTH-5_DD-MM-YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -5, Now), "mmm") & "-" & Year(DateAdd("m", -5, Now)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYMONTH-6_DD-MM-YYYY%", FormatDate("01-" & FormatDate(DateAdd("m", -6, Now), "mmm") & "-" & Year(DateAdd("m", -6, Now)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FIRSTDAYMONTH_DD-MM-YYYY%", FormatDate("01-" & FormatDate(Now, "mmm") & "-" & Year(Now), "dd-mm-yyyy"))
            End If
    If InStr(SubstDateTimeVars, "%LAST") Then
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%LASTDAYMONTH_DD-MM-YYYY%", FormatDate(DateAdd("d", -1, "01-" & FormatDate(DateAdd("m", 1, Now), "mmm") & "-" & Year(DateAdd("m", 1, Now))), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%LASTDAYPREVIOUSMONTH_DD/MM/YYYY%", FormatDate(DateAdd("d", -1, "01/" & Month(Now) & "/" & Year(Now)), "dd/mm/yyyy"))
		SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%LASTDAYPREVIOUSMONTH_MM/DD/YYYY%", FormatDate(DateAdd("d", -1, "01/" & Month(Now) & "/" & Year(Now)), "mm/dd/yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%LASTDAYPREVIOUSMONTH_DD-MM-YYYY%", FormatDate(DateAdd("d", -1, "01-" & Month(Now) & "-" & Year(Now)), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%LASTDAYPREVIOUSMONTH_YYYY-MM-DD%", FormatDate(DateAdd("d", -1, "01-" & Month(Now) & "-" & Year(Now)), "yyyy-mm-dd"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%LASTDAYPREVIOUSMONTH_YYYYMMDD%", FormatDate(DateAdd("d", -1, "01-" & Month(Now) & "-" & Year(Now)), "yyyymmdd"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%LASTDAYPREVIOUSQUARTER_DD-MM-YYYY%", FormatDate(DateAdd("d", -1, DateSerial(Year(NOW), Int((Month(NOW) - 1) / 3) * 3 + 1, 1)), "dd-mm-yyyy"))
            End If
    If InStr(SubstDateTimeVars, "%CURRENT") Then
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE_DD-MM-YYYY%", FormatDate(Now, "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE_DD/MM/YYYY%", FormatDate(Now, "dd/mm/yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-1YEAR_DD-MM-YYYY%", FormatDate(DateAdd("yyyy", -1, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-1MONTH_DD-MM-YYYY%", FormatDate(DateAdd("m", -1, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-6MONTH_DD-MM-YYYY%", FormatDate(DateAdd("m", -6, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-15MONTH_DD-MM-YYYY%", FormatDate(DateAdd("m", -15, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-1DAY_DD-MM-YYYY%", FormatDate(DateAdd("d", -1, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-2DAY_DD-MM-YYYY%", FormatDate(DateAdd("d", -2, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-3DAY_DD-MM-YYYY%", FormatDate(DateAdd("d", -3, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-7DAY_DD-MM-YYYY%", FormatDate(DateAdd("d", -7, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-7DAY_YYYYMMDD%", FormatDate(DateAdd("d", -7, Now), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-8DAY_YYYYMMDD%", FormatDate(DateAdd("d", -8, Now), "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-14DAY_DD-MM-YYYY%", FormatDate(DateAdd("d", -14, Now), "dd-mm-yyyy"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE_PLUS_2DAY_DD-MMM-YYYY%", FormatDate(DateAdd("d", 2, Now), "DD-MMM-YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTWEEK_YYYYWW%", FormatDate(Now, "YYYYWW"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTWEEK-1_YYYYWW%", FormatDate(DateAdd("d", -7, Now), "YYYYWW"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTWEEK-2_YYYYWW%", FormatDate(DateAdd("d", -14, Now), "YYYYWW"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTWEEK-3_YYYYWW%", FormatDate(DateAdd("d", -21, Now), "YYYYWW"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTWEEK-4_YYYYWW%", FormatDate(DateAdd("d", -28, Now), "YYYYWW"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE_PLUS_10YEAR_DD-MM-YYYY%", FormatDate(DateAdd("yyyy", 10, Now), "DD-MM-YYYY"))                
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-1DAY_DD%", FormatDate(DateAdd("d", -1, Now), "DD"))       
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-1DAY_MM%", FormatDate(DateAdd("d", -1, Now), "MM"))      
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-1DAY_MMM%", FormatDate(DateAdd("d", -1, Now), "MMM"))               
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%CURRENTDATE-1DAY_YYYY%", FormatDate(DateAdd("d", -1, Now), "YYYY"))    
            End If
            If InStr(SubstDateTimeVars, "%") Then
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%M/D/YYYY%", FormatDate(Now, "M/D/YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%DD/MM/YYYY%", FormatDate(Now, "DD/MM/YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%DD MMM YYYY%", FormatDate(Now, "dd Mmm YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%D%", FormatDate(Now, "d"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%DD%", FormatDate(Now, "dd"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%DD-MMM-YYYY%", FormatDate(Now, "dd-Mmm-YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%EOM_YYYYMM%", FormatDate(DateAdd("d", -14, Now), "YYYYMM"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%DDMMYYYY%", FormatDate(Now, "DDMMYYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%HH MM SS%", FormatDate(Now, "hh nn ss"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%HH-MM-SS%", FormatDate(Now, "hh-nn-ss"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%HH.MM.SS%", FormatDate(Now, "hh.nn.ss"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%HH:MM:SS%", FormatDate(Now, "hh:nn:ss"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%HHMMSS%", FormatDate(Now, "hhnnss"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYYY-MM-DD-HH-MM-SS%", FormatDate(Now, "YYYY-MM-dd-hh-nn-ss"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%M%", FormatDate(Now, "m"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%MM%", FormatDate(Now, "mm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%MMM%", FormatDate(Now, "Mmm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%MMMM YYYY%", FormatDate(Now, "Mmmm YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%MMMM%", FormatDate(Now, "Mmmm"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YY%", FormatDate(Now, "YY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYWW%", FormatDate(Now, "YYWW"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYYY%", FormatDate(Now, "YYYY"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYYY-MM-DD%", FormatDate(Now, "YYYY-MM-DD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYYY.MM.DD%", FormatDate(Now, "YYYY.MM.DD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYYYMMDD%", FormatDate(Now, "YYYYMMDD"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYYYMM%", FormatDate(Now, "YYYYMM"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYYYMMDD_HHMMSS%", FormatDate(Now, "yyyymmdd_hhnnss"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%YYYYMMDD_HHMM%", FormatDate(Now, "yyyymmdd_hhnn"))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%_MMM%", UCase(FormatDate(Now, "MMM")))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FQUARTERYEAR%", Year(DateAdd("m", -3, Now)))
                SubstDateTimeVars = ReplaceAllOccurences(SubstDateTimeVars, "%FQUARTER%", Int((Month(Now) - 1) / 3))
            End If  
End Function

Function dtAddSubtractBusinessDay(dDate, iDays, sHolidaylist) 
    Dim i
    Dim stepValue
    If iDays <= 0 Then
        stepValue = -1
    Else
        stepValue = 1
    End If
    For i = 0 To iDays Step stepValue
        If i <> 0 Then
            dDate = DateAdd("d", stepValue, dDate)
        End If
        While IntBusinessDay(dDate, sHolidaylist) = 0  ' Loop while no businessday.
            dDate = DateAdd("d", stepValue, dDate)
        Wend
        'MsgBox i & " = " & dDate
    Next 
    dtAddSubtractBusinessDay = dDate
End Function

Public Function IntBusinessDay(dDate, sHolidaylist) 
    'Input: date and an optional holidaylist
    'Purpose: if input date is a businessday, it returns the nth business day of the month,
    'weekends return 0,
    'holidays don't count as businessday and also return 0
    '(input format sHolidaylist=01-01-2006;25-12-2006;)
    'this fucntion can be used to determine if a date is a businessdate (IntBusinessDay()<>0) or check if a date is a specific business date number
    
    If dDate = 0 Then dDate = Now
    IntBusinessDay = 0
    Dim MonthDay(31)
    Dim i

    If WeekDay(dDate, vbMonday) <= 5 And InStr(1, sHolidaylist, FormatDate(dDate, "dd-mm-yyyy")) = 0 Then
        'The inputdate is a weekday and not a holiday,
        'so we can now check the businessday number
        
        'Count the businessdays for this month until the inputdate
        For i = 1 To Day(dDate)
            If WeekDay(DateSerial(Year(dDate), Month(dDate), i), vbMonday) <= 5 And InStr(1, sHolidaylist, FormatDate(DateSerial(Year(dDate), Month(dDate), i), "dd-mm-yyyy")) = 0 Then
                    IntBusinessDay = IntBusinessDay + 1
            End If
        Next 
    End If
End Function

Public Function strBusinessDay(dDate, sHolidaylist)
    Dim nmbr
    nmbr = IntBusinessDay(dDate, sHolidaylist)
    If nmbr = 0 Then
        strBusinessDay = "(no business day)"
    Else
        strBusinessDay = "(business day " & nmbr & ")"
    End If
End Function


Function ListDir (ByVal Path)
            Dim fso: Set fso = CreateObject("Scripting.FileSystemObject")
            If Path = "" then Path = "*.*"
            Dim Parent, Filter
            if fso.FolderExists(Path) then      ' Path is a directory
                        Parent = Path
                        Filter = "*"
            Else
                        Parent = fso.GetParentFolderName(Path)
                        If Parent = "" Then If Right(Path,1) = ":" Then Parent = Path: Else Parent = "."
                        Filter = fso.GetFileName(Path)
                        If Filter = "" Then Filter = "*"
            End If
            ReDim a(10)
            Dim n: n = 0
            Dim Folder: Set Folder = fso.GetFolder(Parent)
            Dim Files: Set Files = Folder.Files
            Dim File
            For Each File In Files
                        If CompareFileName(File.Name,Filter) Then
                                    If n > UBound(a) Then ReDim Preserve a(n*2)
                                    a(n) = File.Path
                                    n = n + 1
                        End If
            Next
            ReDim Preserve a(n-1)
            ListDir = a
End Function


Function CompareFileName (ByVal Name, ByVal Filter) ' (recursive)
            CompareFileName = False
            Dim np, fp: np = 1: fp = 1
            Do
                        If fp > Len(Filter) Then CompareFileName = np > len(name): Exit Function
                        If Mid(Filter,fp) = ".*" Then    ' special case: ".*" at end of filter
                                    If np > Len(Name) Then CompareFileName = True: Exit Function
                        End If
                        If Mid(Filter,fp) = "." Then     ' special case: "." at end of filter
                                    CompareFileName = np > Len(Name): Exit Function
                        End If

                        Dim fc: fc = Mid(Filter,fp,1): fp = fp + 1
                        Select Case fc
                        Case "*"
                                    CompareFileName = CompareFileName2(name,np,filter,fp)
                                    Exit Function
                        Case "?"
                                    If np <= Len(Name) And Mid(Name,np,1) <> "." Then np = np + 1
                        Case Else
                                    If np > Len(Name) Then Exit Function
                                    Dim nc: nc = Mid(Name,np,1): np = np + 1
                                    If Strcomp(fc,nc,vbTextCompare)<>0 Then Exit Function
                        End Select
            Loop
End Function

Function CompareFileName2 (ByVal Name, ByVal np0, ByVal Filter, ByVal fp0)
   Dim fp: fp = fp0
   Dim fc2
   Do                                  ' skip over "*" and "?" characters in filter
      If fp > Len(Filter) Then CompareFileName2 = True: Exit Function
      fc2 = Mid(Filter,fp,1): fp = fp + 1
      If fc2 <> "*" And fc2 <> "?" Then Exit Do
      Loop
   If fc2 = "." Then
      If Mid(Filter,fp) = "*" Then     ' special case: ".*" at end of filter
         CompareFileName2 = True: Exit Function
         End If
      If fp > Len(Filter) Then         ' special case: "." at end of filter
         CompareFileName2 = InStr(np0,Name,".") = 0: Exit Function
         End If
      End If
   Dim np
   For np = np0 To Len(Name)
      Dim nc: nc = Mid(Name,np,1)
      If StrComp(fc2,nc,vbTextCompare)=0 Then
         If CompareFileName(Mid(Name,np+1),Mid(Filter,fp)) Then
            CompareFileName2 = True: Exit Function
            End If
         End If
      Next
   CompareFileName2 = False
End Function

'===============================================================================
' This function reads data from an Excel sheet without using MS-Office
'===============================================================================
Function ReadExcel( myXlsFile, mySheet, my1stCell, myLastCell, blnHeader )
' Arguments:
' myXlsFile   [string]   The path and file name of the Excel file
' mySheet     [string]   The name of the worksheet used (e.g. "Sheet1")
' my1stCell   [string]   The index of the first cell to be read (e.g. "A1")
' myLastCell  [string]   The index of the last cell to be read (e.g. "D100")
' blnHeader   [boolean]  True if the first row in the sheet is a header
'
' Returns:
' The values read from the Excel sheet are returned in a two-dimensional
' array; the first dimension holds the columns, the second dimension holds
' the rows read from the Excel sheet.
'
' Written by Rob van der Woude
' http://www.robvanderwoude.com
    Dim arrData( ), i, j
    Dim objExcel, objRS
    Dim strHeader, strRange

    Const adOpenForwardOnly = 0
    Const adOpenKeyset      = 1
    Const adOpenDynamic     = 2
    Const adOpenStatic      = 3

    ' Define header parameter string for Excel object
    If blnHeader Then
        strHeader = "HDR=YES;"
    Else
        strHeader = "HDR=NO;"
    End If

    ' Open the object for the Excel file
    Set objExcel = CreateObject( "ADODB.Connection" )
    objExcel.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
                  myXlsFile & ";Extended Properties=""Excel 8.0;" & _
                  strHeader & """"

    ' Open a recordset object for the sheet and range
    Set objRS = CreateObject( "ADODB.Recordset" )
    strRange = mySheet & "$" & my1stCell & ":" & myLastCell
    objRS.Open "Select * from [" & strRange & "]", objExcel, adOpenStatic

    ' Read the data from the Excel sheet
    i = 0
    Do Until objRS.EOF
        ' Stop reading when an empty row is encountered in the Excel sheet
        If IsNull( objRS.Fields(0).Value ) Or Trim( objRS.Fields(0).Value ) = "" Then Exit Do
        ' Add a new row to the output array
        ReDim Preserve arrData( objRS.Fields.Count - 1, i )
        ' Copy the Excel sheet's row values to the array "row"
        ' IsNull test credits: Adriaan Westra
        For j = 0 To objRS.Fields.Count - 1
            If IsNull( objRS.Fields(j).Value ) Then
                arrData( j, i ) = ""
            Else
                arrData( j, i ) = Trim( objRS.Fields(j).Value )
            End If
        Next
        ' Move to the next row
        objRS.MoveNext
        ' Increment the array "row" number
        i = i + 1
    Loop

    ' Close the file and release the objects
    objRS.Close
    objExcel.Close
    Set objRS    = Nothing
    Set objExcel = Nothing

    ' Return the results
    ReadExcel = arrData
End Function

'=========================================================================
' Strip Off Last Slash from folder to prevent errors
'=========================================================================
Function StripOffLastSlash(strFolderName)
            If Right(strFolderName,1)="\" Then
                        StripOffLastSlash = Left(strFolderName,Len(strFolderName)-1)         
            Else
                        StripOffLastSlash = strFolderName
            End If
End Function

'=========================================================================
' Strip Off filename from folder c:\test\test.html => test.html
'=========================================================================
Function GetFileName(path_file)
            If InStrRev(strLogfile, "\", -1, 1) > 0 Then
                        GetFileName = Mid(path_file,InStrRev(path_file, "\", -1, 1)+1,Len(path_file))
            Else
                        GetFileName = path_file          
            End If  
End Function

'===============================================================================
' ReportStatusList handling
'===============================================================================
Sub OpenReportStatusList
            If objFS.FileExists(processFolder & "\ReportStatusList.xml") Then
                        Set ReportStatusList = CreateObject("ADOR.Recordset")
                        ReportStatusList.Open processFolder & "\ReportStatusList.xml"
            Else
                        WriteLineToFile strLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";ReportStatusList could not be opened"
            End If
End Sub

Sub SaveCloseReportStatusList
            If objFS.FileExists(processFolder & "ReportStatusList.xml") Then
                        objFS.DeleteFile(processFolder & "ReportStatusList.xml")
            End If
            ReportStatusList.Filter = ""
            ReportStatusList.Save processFolder & "ReportStatusList.xml", 1
            Set ReportStatusList = Nothing
End Sub

Function SetReportStatus(LocalID,Status,RunID,Dest1OK,Dest2OK,EmailOK,PrintOK,FinishedOK)
            'Update the ADOR object
            'The OK fields will take values: 
            '0 = do not change , 1= set to now, 99 = clear
            '
            ReportStatusList.Filter = "LocalID='" & LocalID & "'"
            If ReportStatusList.RecordCount > 0 Then
                        ReportStatusList.MoveFirst
                        Do Until ReportStatusList.EOF
                                    If Status <> "" Then 
                                                ReportStatusList("Status") = Status
                                    End If
                                    If RunID <> "" Then 
                                                ReportStatusList("RunID") = RunID
                                    End If
                                    If ReportStatusList.Fields.Item("Dest1OK") <> "N/A" Then
                                                If Dest1OK = 1 Then ReportStatusList("Dest1OK") = FormatDate(Now(), "yyyymmdd_hhnnss") End If
                                                If Dest1OK = 99 Then ReportStatusList("Dest1OK") = "" End If
                                    End If
                                    If ReportStatusList.Fields.Item("Dest2OK") <> "N/A" Then
                                                If Dest2OK = 1 Then ReportStatusList("Dest2OK") = FormatDate(Now(), "yyyymmdd_hhnnss") End If
                                                If Dest2OK = 99 Then ReportStatusList("Dest2OK") = "" End If
                                    End If
                                    If ReportStatusList.Fields.Item("EmailOK") <> "N/A" Then
                                                If EmailOK = 1 Then ReportStatusList("EmailOK") = FormatDate(Now(), "yyyymmdd_hhnnss") End If
                                                If EmailOK = 99 Then ReportStatusList("EmailOK") = "" End If
                                    End If
                                    If ReportStatusList.Fields.Item("PrintOK") <> "N/A" Then
                                                If PrintOK = 1 Then ReportStatusList("PrintOK") = FormatDate(Now(), "yyyymmdd_hhnnss") End If
                                                If PrintOK = 99 Then ReportStatusList("PrintOK") = "" End If
                                    End If
                                    If FinishedOK = 1 Then ReportStatusList("FinishedOK") = FormatDate(Now(), "yyyymmdd_hhnnss") End If
                                    If FinishedOK = 99 Then ReportStatusList("FinishedOK") = "" End If
                                    ReportStatusList.Update
                                    ReportStatusList.MoveNext
                        Loop
                        SetReportStatus=RunID & ";SetReportStatus updated report " & LocalID & " with status " & Status
            Else
                        SetReportStatus=RunID & ";SetReportStatus NOT updated report " & LocalID 
            End If  
End Function   

'=============================================================
' DeleteEmptyFolders(path,ook de rootfolder als hij leeg is)
'=============================================================
Sub DeleteEmptyFolders(sPath, bDeleteThisFolder,strLogfile)
            Set folder = objFS.getfolder(sPath)
            'recurse first...
            For each fldr in folder.subfolders
                        DeleteEmptyFolders fldr.path,True,strLogfile
            Next
            
            'if no files or folders then delete...
            'bDeleteThisFolder is false for the root of the subtree, and true for sub-folders (unless you want to delete the entire subtree if it is empty).
            If (folder.files.count = 0) and (folder.subfolders.count) = 0 and bDeleteThisFolder Then
                        On Error Resume Next
                        logmessage = folder.Path
                        folder.Delete    
                        If strLogfile<>"" Then
                                    If Err.Number Then
                                                WriteLineToFile strLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";" & logmessage & " NOT deleted: " & Err.Description & " (" & Err.Number & ")"
                                    Else
                                                WriteLineToFile strLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";" & logmessage & " deleted"
                                    End If  
                        End If  
                        On Error Goto 0            
                        Exit Sub
            End If

End Sub

'=============================================================
' DeleteFolder_Older_Than_X_Days (path,days,logfile)
'=============================================================
Sub DeleteFolder_Older_Than_X_Days(sPath, NoOfDays,strLogfile)
            Set folder = objFS.getfolder(sPath)
            'recurse first...
            For each fldr in folder.subfolders
                        DeleteFolder_Older_Than_X_Days fldr.path,NoOfDays,strLogfile
            Next

            If Now()-folder.DateCreated > NoOfDays Then
                        On Error Resume Next
                        logmessage=folder.Path & " last modified on " & folder.DateCreated 
                        folder.Delete
                        If strLogfile<>"" Then
                                    If Err.Number Then
                                                WriteLineToFile strLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";" & logmessage & " NOT deleted: " & Err.Description & " (" & Err.Number & ")"
                                    Else
                                                WriteLineToFile strLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";" & logmessage & " deleted"
                                    End If  
                        End If  
												On Error Goto 0
                        Exit Sub
            End If
End Sub


'=============================================================
' DeleteFile_Older_Than_X_Days (path,days,logfile)
'=============================================================
Sub DeleteFile_Older_Than_X_Days(sPath, NoOfDays,strLogfile)
            Set folder = objFS.getfolder(sPath)
            'recurse first...
            For each fldr in folder.subfolders
                        DeleteFile_Older_Than_X_Days fldr.path,NoOfDays,strLogfile
            Next

            If folder.files.count > 0 Then
                        For Each objFile in folder.Files
                                    If Now()-objFile.DateLastModified > NoOfDays Then
                                                On Error Resume Next
                                                logmessage=objFile.Name & " created on " & objFile.DateLastModified
                                                objFile.Delete
                                                If strLogfile<>"" Then
                                                            If Err.Number Then
                                                                        WriteLineToFile strLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";" & logmessage & " NOT deleted: " & Err.Description & " (" & Err.Number & ")"
                                                            Else
                                                                        WriteLineToFile strLogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";" & logmessage & " deleted"
                                                            End If  
                                                End If  
                                                On Error Goto 0
                                    End If              
                        Next
            End If
End Sub
'=============================================================
' Send mail with attachment using Blat
'=============================================================

Sub SendSMTPMailAttachmentWithBlat (emailfrom, emailto, subject, textbody, attachmentfolder, attachmentfile)
	On Error Resume Next
	
	Dim returnvalue
	Dim sBlatParams:sBlatParams = ""
	Dim sBlatFolder:sBlatFolder = StripOffLastSlash(GetIni ("General", "blatfolder", "" , sAllIniFile))
	Dim sBlatLog:sBlatLog = logFolder & "\" & FormatDate(Now, "yyyymmdd") & "_blat.log"
	
	Dim objFSB,objFileB
  Dim sTempFileName
  Set objFSB = CreateObject("Scripting.FileSystemObject")
  sTempFileName = objFSB.GetTempName()
  

	
	If emailfrom="" Or emailto="" Or subject="" Or textbody="" Or attachmentfolder="" Or attachmentfile = "" Then Exit Sub End If
	'create temporary file in the loglocation
	Set objFileB = objFSB.CreateTextFile(logFolder & "\" & sTempFileName)
  objFileB.Write textbody
  objFileB.Close
  Set objFileB = Nothing
  
	
	'emailto can contain ; as email separator. Blat needs the comma
	Dim attachment:attachment = attachmentfolder & attachmentfile
	sBlatParams = sBlatParams & chr(34) & ReplaceAllOccurences(emailto,";",",") & chr(34) & " "
	sBlatParams = sBlatParams & chr(34) & subject & chr(34) & " " 
	'the textbody can contain the <br> html sequence for CRLF. Blat uses the pipe char for this
	'sBlatParams = sBlatParams & chr(34) & ReplaceAllOccurences(textbody,"<br>","|") & chr(34) & " " 
	sBlatParams = sBlatParams & chr(34) & logFolder & "\" & sTempFileName & chr(34) & " " 
	If objFS.FileExists(attachment) Then
	'attachment filename can contain , in the name. Blat needs to know it is a comma by using the escape character \
		sBlatParams = sBlatParams & chr(34) & attachment & chr(34) & " "	
	Else
		On Error Goto 0
		Exit Sub
	End If
	sBlatParams = sBlatParams & chr(34) & sBlatLog & chr(34)
	
	WL strLogfile, Section, "EMAIL", "Start SendSMTPMailAttachment (mailfrom: " & emailfrom & ")"
	WL strLogfile, Section, "EMAIL", "Start SendSMTPMailAttachment (mailto: " & emailto & ")"
	WL strLogfile, Section, "EMAIL", "Start SendSMTPMailAttachment (subject: " & subject & ")"
	WL strLogfile, Section, "EMAIL", "Start SendSMTPMailAttachment (body: " & Left(textbody,30) & "... )"
	WL strLogfile, Section, "EMAIL", "Start SendSMTPMailAttachment (attachment: " & attachment & " )"
	WL strLogfile, Section, "EMAIL", "Blat commandline: " & sBlatFolder & "\blatbatch.cmd " & sBlatParams
	
	returnvalue = WshShell.RUN (sBlatFolder & "\blatbatch.cmd " & sBlatParams, 0, True)
	
	If returnvalue <> 0 Then
        WriteLineToFile strGroupTriggerLogfile , FormatDate(Now(), "yyyymmdd_hhnnss") & ";Error sending email with att. to: " & emailto & ": " & subject & " Error: " & Err.Description & ", number: " & Err.Number& ", attachment: " & attachment & ", source: " & Err.Source
        WL strLogfile, Section, "ERROR", "Error sending email with att. to: " & emailto & ": " & subject & " Error: " & Err.Description & ", number: " & Err.Number & ", attachment: " & attachment & ", source: " & Err.Source
        'copy attachment to email folder
        errorFolderEmail = errorFolder & "\" & emailto
        If CheckFolder(errorFolderEmail) Then
        'copy attachment to errorFolderEmail
        	If objFS.FileExists(attachment) Then
        		objFS.CopyFile attachment, errorFolderEmail & attachmentfile
        			WL strLogfile, Section, "ERROR", "Copied attachment from " & attachment & " to: " & errorFolderEmail & attachmentfile
        	End If
        Else
        	WL strLogfile, Section, "ERROR", "Failed to create errorfolder for: " & emailto
        	'copy to error folder root
        	If objFS.FileExists(attachment) Then
        		objFS.CopyFile attachment, errorFolder & attachmentfile
        			WL strLogfile, Section, "ERROR", "Copied attachment from " & attachment & " to: " & errorFolderEmail & attachmentfile
        	End If
        End If
        Err.Clear
    Else
        WriteLineToFile strGroupTriggerLogfile , FormatDate(Now(), "yyyymmdd_hhnnss") & ";Email with att. sent to: " & emailto & ": " & subject
        WL strLogfile, Section, "EMAIL", "Email with att. sent to: " & emailto & ": " & subject
				'remove temp html bodytext
				objFSB.DeleteFile logFolder & "\" & sTempFileName, True
    End If

		Set objFSB = Nothing
		Set objEmail = Nothing
		On Error Goto 0
End Sub
Function WL(p_strlogfile,p_strsection,p_strkind,p_strmessage)
'============================================================
'Standardised messages (date, section, kind of message, message)
'Filtering on type of message (e.g. DEBUG, INFO) 
'sIniFile needs to be set in calling program
'============================================================

	Dim inris
	Dim bPrint:bPrint = False
	Dim sLogLevel:sLogLevel = getini("General", "loglevel", "DEBUG", sIniFile)	
	If sLogLevel = "DEBUG" Then
		bPrint = True
	Else
		If p_strkind = "DEBUG" Then
			bPrint = False
		Else
			bPrint = True
		End If
	End If
	If p_strkind = "ERROR" Then
		bErrorInLogFile = 1
	End If
		
	inris = 20 - Len(p_strkind)
	If inris < 0 Then 
		inris = 0
	End If
	p_strkind = p_strkind & RepeatString ("=", inris)
	
	If bPrint Then
		WriteLineToFile p_strlogfile, FormatDate(Now(), "yyyymmdd_hhnnss") & ";" & p_strsection & ";" & p_strkind & ";" & p_strmessage
	End If
	

End Function

Function RepeatString(str, number) 
	Dim arrTmp()
	If number <= 0 Then
		RepeatString = ""
	Else
		ReDim arrTmp(number)
		RepeatString = Join(arrTmp, str)
	End If
End Function
Function FMTTXT(textinput)
	FMTTXT=Left(textinput & "                     ",15)
End Function
Function CloneAndFilter(rstData)

Dim fld
Dim rst
Dim lngFldCount
Dim bReportStatusList
   
    'Create a recordset object
    Set rst = CreateObject("ADOR.Recordset")
    'If this is the ReportStatusList.xml
    bReportStatusList = 0
    For Each fld In rstData.Fields
   		If fld.Name = "FinishedOK" Then
   			bReportStatusList = 1
   		End If
   	Next
    
    'Copy the field definition
    For Each fld In rstData.Fields
    	If bReportStatusList = 0 Then
        	rst.Fields.Append fld.Name, fld.Type, fld.DefinedSize, fld.Attributes
        	If fld.Precision > 0 Then
            	rst.Fields(fld.Name).Precision = fld.Precision
        	End If
        	If fld.NumericScale > 0 Then
            	rst.Fields(fld.Name).NumericScale = fld.NumericScale
        	End If

    	Else
    		'add filter for ReportStatusList00
    		If fld.Name = "Attempt" _
    				or fld.Name = "RunID" _
    				or fld.Name = "Report" _
    				or fld.Name = "Prio" _ 
    				or fld.Name = "Started" _ 
    				or fld.Name = "FinishedOK" _ 
    				or fld.Name = "BO_Output" _ 
    				or fld.Name = "Prompts" _ 
    				or fld.Name = "Destination1" _ 
    				or fld.Name = "Destination2" Then
        	rst.Fields.Append fld.Name, fld.Type, fld.DefinedSize, fld.Attributes
        	If fld.Precision > 0 Then
            	rst.Fields(fld.Name).Precision = fld.Precision
        	End If
        	If fld.NumericScale > 0 Then
            	rst.Fields(fld.Name).NumericScale = fld.NumericScale
        	End If
        End If
      End If
    Next
    
    'Use a client cursor
    rst.CursorLocation = 3 'adUseClient
    'Open the recordset
    rst.Open , , 1 'adOpenKeyset
    
    If Not (rstData.EOF And rstData.BOF) Then
        rstData.MoveFirst
    End If
    
    'loop through the source recordset and copy the data
    Do While Not rstData.EOF
        'Add a new records
        rst.AddNew
        If bReportStatusList = 1 Then
        	For Each fld In rstData.Fields
            		'add filter for ReportStatusList
    				If fld.Name = "Attempt" _
    					or fld.Name = "RunID" _
    					or fld.Name = "Report" _
    					or fld.Name = "Prio" _ 
    					or fld.Name = "Started" _ 
    					or fld.Name = "FinishedOK" _ 
    					or fld.Name = "BO_Output" _ 
    					or fld.Name = "Prompts" _ 
    					or fld.Name = "Destination1" _ 
    					or fld.Name = "Destination2" Then
    				 		rst.Fields(fld.Name).Value = rstData.Fields(fld.Name).Value
           	End If
        	Next
        Else
        	For Each fld in rstData.Fields
        		rst.Fields(fld.Name).Value = rstData.Fields(fld.Name).Value
        	Next
      	End If
 
        'Next record
        rstData.MoveNext
    Loop
    
    'If there was data to roll through,
    If rst.RecordCount > 0 Then
        'move to the begining of the source recordset
        rst.MoveFirst
    End If
    
    'Return the clone
    Set CloneAndFilter = rst
    'objFS.DeleteFile processFolder & "\rst.xml"
    'rst.Save processFolder & "\rst.xml", 1
    'Release objects
    Set rst = Nothing
    Set fld = Nothing

    Exit Function


End Function
'=========================================================================
' Open XML recordset from a full path and return html table from it..
'=========================================================================
Function XMLRecordsetToHTMLTable(rs,title)
	Dim DataList:Set DataList = CreateObject("ADOR.Recordset")
	DataList.Open rs
	'filter infopage
	Set DataList = CloneAndFilter(DataList)
	Dim res, fld, tmp

	res = TitleToHTML(title)

	' prepare the <TABLE> tag
	res = res & "<DIV><TABLE ""BORDER=1"" STYLE=""font-family: Verdana,Arial;font-size:10px;text-align:left;BORDER: #88AACC 1px solid; border-collapse: collapse; PADDING: 4px; BACKGROUND-COLOR: #f2f3f4""><HEAD>"
	For Each fld In DataList.Fields
		res = res & "<TD ALIGN=CENTER STYLE=""BORDER: #a7b4ce 1px solid;background-color:#030099;color:#FFF""><B>" & fld.Name & "</B></TD>"
	Next
	res = res & "</HEAD>"

	' get all the records in a semi-formatted string
	tmp = DataList.GetString(, , "</TD><TD ALIGN=CENTER STYLE=""BORDER: #a7b4ce 1px solid;"">", "</TD></TR><TR><TD ALIGN=CENTER STYLE=""BORDER: #a7b4ce 1px solid;"">", "")
	' strip what has been appended to the last cell of the last row
	tmp = Left(tmp, Len(tmp) - Len("<TR><TD ALIGN=CENTER STYLE=""BORDER: #a7b4ce 1px solid;"">"))

	' add opening tags to the first cell of the first row of the table and complete the table
	XMLRecordsetToHTMLTable = res & "<TR><TD STYLE=""BORDER: #a7b4ce 1px solid;"">" & tmp & "</TABLE></DIV>"
	Set DataList = Nothing
End Function

'=========================================================================
' Create title html bar..
'=========================================================================
Function TitleToHTML(title)
  	If InStr(title,"Error") Then
  		TitleToHTML = "<table style=""border-spacing:10px;""><tr><td style=""width:50px;background-color:#CC0000;padding:0px;"">"
  		TitleToHTML = TitleToHTML & "<div style=""font-family: Wingdings,Verdana,Arial;color:#FFFFFF;font-size:48px;"">L</div></td>"
  	Else
  		TitleToHTML = "<table style=""border-spacing:10px;""><tr><td style=""width:50px;background-color:#88AACC;padding:0px;"">"
  		TitleToHTML = TitleToHTML & "<div style=""font-family: WebDings,Verdana,Arial;color:#FFFFFF;font-size:48px;"">i</div></td>"
  	End If
	TitleToHTML = TitleToHTML & "<td style=""width:400px;background-color:#030099;padding:5px;""><div style=""font-family: Verdana,Arial;color:#FFF;font-size:18px;vertical-align:bottom;"">" & title & "</div></td></tr></table><br />"
End Function

